---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Manage groups

Use Groups to manage one or more related projects at the same time.

For instructions on how to view, create, and manage groups, see [Groups](index.md).
